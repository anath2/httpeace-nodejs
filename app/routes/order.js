let mongoose = require('mongoose')
let Order  = require('../models/order')

/*
    GET ALL ORDERS
*/
const getOrderList = (req, res) => {
    let query = Order.find({})
    query.exec()
    .catch((err) => {
        err && res.send(err)
    })
    .then((orders) => res.json(orders))    
} 

const createOrder = (req, res) => {
    let newOrder = new Order(req.body) 
    newOrder.save()
    .catch((err) => {
        err && res.send(err)
    })
    .then((orders) => res.json(orders))
}

const getOrder = (req, res) => {
    let query = Order.findById(req.params.id)
    query.exec()
    .catch((err) => {
        err && res.send(err)
    })
    .then((order) => res.json(order))
}

const deleteOrder = (req, res) => {
    let query = Order.remove({ _id: req.params.id })
    query.exec()
    .catch((err) => {
       err && res.send(err)
    })
    .then((order) => res.send({ message: "order deleted successfully", order }))
}

const updateOrder = (req, res) => {
    let query = Order.findById(req.params.id)
    query.exec()
    .catch((err) => {
        err && res.send(err)
    })
    .then((order) => {
        let newOrder = Object.assign({}, order, req.body)
        newOrder.save()
        .catch((err) =>  {
            err && res.send(err)
        })
        .then((orders) => res.json({ message: "order updated successfully", orders }))
    })
}

module.exports = {
    getOrderList,
    createOrder,
    getOrder,
    updateOrder,
    deleteOrder
}

// TODO : Use ES6 promises instead