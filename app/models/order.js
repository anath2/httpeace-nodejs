let mongoose = require('mongoose')

let Schema = mongoose.Schema

let detailSchema = new Schema({
    
    detailStatus: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    completedDate: {
        type: Date,
        default: Date.now()
    },
    product: {
        options: {
            quantity: {
                type: Number
            },
            page: {
                type: String
            },
            size: {
                type: String,
                enum: ['A3', 'A4']
            }
        }
    }                
})

let orderSchema = new Schema(
    {
        orderNo: {
            type: String,
            required: true
        },
        orderDate: {
            type: Date,
            default: Date.now
        },
        currency: {
            type: String,
            default: "HKD"
        },
        totAmount: {
            type: Number,
            required: true,
            min: 0
        },
        orderStatus: {
            type: Number,
            default: 0
        },
        paymentStatus: {
            type: Number,
            default: 0
        },
        details: {
            type: [detailSchema],
            required: true
        },
        address: {
            cosignee: {
                type: String,
                required: true
            },
            country: {
                type: String,
                required: true
            },
            address: {
                type: String,
                required: true
            }
        }
    }
)

orderSchema.pre('save', (next) => {
    let now = new Date()
    if(!this.orderDate) {
        this.orderDate = now
    }
    next();
})

module.exports = mongoose.model('order', orderSchema)