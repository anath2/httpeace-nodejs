let express = require('express');
let mongoose = require('mongoose');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let config = require('config');

let app = express();
let port  = 8080;
let order = require('./app/routes/order')

let dbParams = { 
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } 
}; 

mongoose.connect(config.get("DBHost"), dbParams);
mongoose.Promise = global.Promise

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

if(config.util.getEnv('NODE_ENV') !== 'test') {
    app.use(morgan('combined')); 
}


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))
app.use(bodyParser.json( {type: 'application/json' } ))


app.get('/', (req, res) => {
    res.send('Welcome to orders API')
})

app.route('/order')
    .get(order.getOrderList)
    .post(order.createOrder)

app.route('/order/:id')
    .get(order.getOrder)
    .put(order.updateOrder)
    .delete(order.deleteOrder)

app.listen(port, 'localhost', () => {
    console.log("Listening on port 8080")
})

module.exports = app 