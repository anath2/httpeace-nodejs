process.env.NODE_ENV = 'test';

let mongoose = require('mongoose')
let chai = require('chai')
let chaiHttp = require('chai-http')

let server = require('../server')
let Order = require('../app/models/order')

let should = chai.should()

chai.use(chaiHttp);

// describe("Orders", () => {
//     beforeEach((done) => {
//         Order.remove({}).exec()
//         .catch((err) => {
//             err && res.send(err)    
//         })
//         .then( 
//             (result) => { 
//                 console.log("The database now empty") 
//                 done();
//             }        
//         )
//     })
// })

describe('Orders', () => {
    beforeEach((done) => {
        Order.remove({}, (err) => { 
           done();         
        });     
    });

    describe("/GET order", () => {
        it('Should GET all orders', (done) => {
            chai.request(server)
            .get('/order')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('array')
                res.body.length.should.be.eql(0)
                done()
            })
        })  
    })

    describe('/POST order', () => {
        it('it should POST the order successfully', (done) => {
            let order = {        
                orderNo: "1903-23432",
                totAmount: 123.34,
                details: [{
                    product: {
                        options: {
                            quantity: 1,
                            page: "8pp",
                            size: "A3"
                        }
                    } 
                }],
                address: {
                    cosignee: "A NATH",
                    country: "Hong Kong",
                    address: "SHATIN"
                },
                paymentStatus: 0,
                orderStatus: 1,
                currency: "HKD"
            }
            chai.request(server)
            .post('/order')
            .send(order)
            .end((err, res) => {              
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.not.have.property('errors');
                done();
            });
        });
    });

    describe('/POST order', () => {
        it('it should not POST an order with totAmount less than zero', (done) => {
            let order = {        
                orderNo: "1903-23432",
                totAmount: -123.34,
                details: [{
                    product: {
                        options: {
                            quantity: 1,
                            page: "8pp",
                            size: "A3"
                        }
                    } 
                }],
                address: {
                    cosignee: "A NATH",
                    country: "Hong Kong",
                    address: "SHATIN"
                },
                paymentStatus: 0,
                orderStatus: 1,
                currency: "HKD"
            }
            chai.request(server)
            .post('/order')
            .send(order)
            .end((err, res) => {              
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('totAmount')
                res.body.errors.totAmount.should.have.property('name').eql('ValidatorError')
                done();
            });
        });
    });

    describe('/POST order', () => {
        it('it should not POST an order with product size not being A4 or A5', (done) => {
            let order = {        
                orderNo: "1903-23432",
                totAmount: 123.34,
                details: [{
                    product: {
                        options: {
                            quantity: 1,
                            page: "8pp",
                            size: "A6"
                        }
                    } 
                }],
                address: {
                    cosignee: "A NATH",
                    country: "Hong Kong",
                    address: "SHATIN"
                },
                paymentStatus: 0,
                orderStatus: 1,
                currency: "HKD"
            }
            chai.request(server)
            .post('/order')
            .send(order)
            .end((err, res) => {              
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                let errors = res.body.errors
                errors['details.0.product.options.size'].should.have.property('name').eql('ValidatorError')
                done();
            });
        });
    });
})